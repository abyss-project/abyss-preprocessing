#!/usr/bin/env bash
#load python 3.6 
. /appli/bioinfo/python/3.6/env.sh
#run extract script on config file
python $HOME/abyss/abyss-preprocessing/extract.py --config-file $HOME/abyss/abyss-preprocessing/extract.ini
#unload python 3.6
. /appli/bioinfo/python/3.6/delenv.sh
