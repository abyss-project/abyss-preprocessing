#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Extract forward and reverse reads from Genoscope raw sequencing file

import sys
import os
import os.path
import subprocess
import argparse
import datetime
import configparser
import logging
import re
import fnmatch
import time

def runcmd(cmd):
    logger.debug("Running process....")
    p=subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, env=os.environ)
    logger.debug(cmd)
    p.wait()
    (out, err)=p.communicate()
    if isinstance(out, bytes):
        out=out.decode("utf-8")
    if p.returncode != 0 and err is not None:
        logger.debug("p = {} -- OUT = {} -- ERR = {}".format(str(p.returncode), out, err))
        logger.debug("ERROR : Process aborted")
        sys.exit(1)
    else:
        logger.debug("Process OK")
        return out

def handleArgs():
    # Arguments definition
    parser = argparse.ArgumentParser("Handle Genoscope raw sequencing file")
    parser.add_argument("--config-file", dest="configfile",
                            help="Path to configuration file", metavar="PATH", required=True)
    # Arguments initialization
    args = parser.parse_args()
    configfile = args.configfile
    if not os.path.isfile(configfile):
            parser.error("Config file {} does not exist".format(configfile))
            sys.exit(1)

    return configfile


if __name__ == '__main__':
    # Handle script arguments
    configfile = handleArgs()

    #parse config file 
    config = configparser.ConfigParser(os.environ)
    config.read(configfile)
    indir = config["common"]["indir"]
    output = config["common"]["output"]
    properties = config["common"]["properties"]
    barcode = config["common"]["barcode"]
    directory = config["common"]["directory"]
    trimreads = config["common"]["trimreads"]

    # logging config
    if not os.path.isdir(output):
        os.makedirs(output)
    logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.FileHandler("{0}/main.log".format(output))
    ])

    logger = logging.getLogger()

    logger.debug("BEGINNING PROCESS !")

    tmpdir= "{}/logs".format(output)
    if not os.path.isdir(tmpdir):
        os.makedirs(tmpdir)
    #check job number
    check=""
    #select input files
    readsfiles=os.listdir(indir)
    if any("fastq.gz" in f for f in readsfiles):
        #list of jobs
        jobs=[]
        # Keep only fastq files type
        readsfiles=[reads for reads in readsfiles if "fastq.gz" in reads]
        treated=[]
        patternGenoscope1 = re.compile("(.)+_[1-9]_[1]_(.)+fastq.gz")
        patternGenoscope2 = re.compile("(.)+_[1-9]_[2]_(.)+fastq.gz")
        patternClassique = re.compile("(.)+R[1-2].fastq.gz")
        counter=0 
        for rf in readsfiles :
            if rf not in treated :
                counter=counter+1
                if patternGenoscope1.match(rf):
                    R1 = rf 
                    if '_1_1_' in str(rf):
                        R2 = rf.replace('_1_1_', '_1_2_')
                    elif '_1_' in str(rf) :
                        R2 = rf.replace('_1_', '_2_')
                elif patternGenoscope2.match(rf):
                    R2 = rf 
                    if '_2_2_' in str(rf):
                        R1 = rf.replace('_2_2_', '_2_1_')
                    elif '_2_' in str(rf) :
                        R1 = rf.replace('_2_', '_1_')
                elif patternClassique.match(rf):
                    if "R2" in rf :
                       R2 = rf 
                       R1 = rf.replace('R2', 'R1')
                    else :
                       R1 = rf
                       R2 = rf.replace('R1', 'R2')
                else :
                    logger.debug("File {} does not match patterns .R[1-2].fastq.gz or _[1-9]_[1-2]XXX.fastq.gz".format(rf))

                logger.debug("R1 = {}".format(R1))
                logger.debug("R2 = {}".format(R2))
                treated.append(R1)
                treated.append(R2)
                #submit extract job
                os.environ['directory']=directory
                os.environ['indir']=indir
                os.environ['R1']=R1
                os.environ['R2']=R2
                os.environ['output']=output
                os.environ['barcode']=barcode
                os.environ['properties']=properties
                os.environ['tmpdir']=tmpdir
                os.environ['trimreads']=trimreads

                cmd="qsub -o {}/logs -V {}/extractR1R2.pbs".format(output, directory)
                
                out = runcmd(cmd)
                jobs.append(out.strip().split(".")[0])
                logger.debug("job number : {}".format(out))
        #check if number of generated files is ok
        cmd="qsub -o {}/logs -V -W depend=afterok:{} {}/check.pbs".format(output,":".join(jobs), directory)
        check = runcmd(cmd).strip().split(".")[0]
        sys.stdout.write(check)
    else :
        logging.debug("No fastq.gz files found in {}, exiting program".format(indir))
        sys.exit(1)
    logger.debug("All subjobs created, check status with qstat -u your_login!")
    sys.exit(check)
