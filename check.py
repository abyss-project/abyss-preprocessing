#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Check if number of files is correct at the end of processing and produce .tar archive of preprocessed samples for use in FROGS without DADA2

import sys
import os
import os.path
import subprocess
import argparse
import datetime
import configparser
import logging
import re
import fnmatch
import time

def runcmd(cmd):
    logging.debug("Running process....")
    p=subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, env=os.environ)
    logging.debug(cmd)
    p.wait()
    (out, err)=p.communicate()
    if isinstance(out, bytes):
        out=out.decode("utf-8")
    if p.returncode != 0 and err is not None:
        logging.debug("p = {} -- OUT = {} -- ERR = {}".format(str(p.returncode), out, err))
        logging.debug("ERROR : Process aborted")
        sys.exit(1)
    else:
        logging.debug("Process OK")
        return out

def handleArgs():
    # Arguments definition
    parser = argparse.ArgumentParser("Handle Genoscope raw sequencing file")
    parser.add_argument("--config-file", dest="configfile",
                            help="Path to configuration file", metavar="PATH", required=True)
    # Arguments initialization
    args = parser.parse_args()
    configfile = args.configfile
    if not os.path.isfile(configfile):
            parser.error("Config file {} does not exist".format(configfile))
            sys.exit(1)

    return configfile


if __name__ == '__main__':
    # Handle script arguments
    configfile = handleArgs()

    #parse config file 
    config = configparser.SafeConfigParser(os.environ)
    config.read(configfile)
    indir = config["common"]["indir"]
    output = config["common"]["output"]

    # logging config
    if not os.path.isdir(output):
        os.makedirs(output)
    logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.FileHandler("{0}/check.log".format(output))
    ])

    logger = logging.getLogger()

    logger.debug("BEGINNING CHECKING PROCESS")

    # Verify number of files in the input directory versus output directory : (output = input *5)
    inputnb = len(fnmatch.filter(os.listdir(indir),'*.fastq.gz'))
    outputnb = len(fnmatch.filter(os.listdir(output),'*.fastq.gz'))
    logger.debug("Input files number : {}, output files number = {}".format(inputnb, outputnb))
    if not int(outputnb / 5) == int(inputnb) :
        logger.debug("Files are missing in the output directory, please check the process")
        sys.exit(1)
    else :
        logger.debug("Number of produced files is OK")

    # create archive for frogs
    cmd="cd {}/frogs; tar cvf samples.tar *_R*.fastq.gz && rm {}/frogs/*_R*.fastq.gz".format(output,output)
    out = runcmd(cmd)

    logger.debug("END OF CHECKING PROCESSS")
