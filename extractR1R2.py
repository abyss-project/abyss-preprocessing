#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Extract forward and reverse reads from Genoscope raw sequencing file

import configparser
import sys
import os
import re
import argparse
import logging
import subprocess
import csv
import datetime
from collections import Counter


def handleArgs():
    # Arguments definition
    parser = argparse.ArgumentParser(
        "Extract forward and reverse reads from Genoscope raw sequencing file")
    parser.add_argument("--input-R1", dest="inR1",
                        help="Path to Genoscope raw reads directory to parse", metavar="PATH", required=True)
    parser.add_argument("--input-R2", dest="inR2",
                        help="Path to Genoscope raw reads directory to parse", metavar="PATH", required=True)
    parser.add_argument("--output-dir", dest="outdir",
                        help="Path to output directory", metavar="PATH", required=True)
    parser.add_argument("--barcode", dest="barcode",
                        help="Barcode marker", required=True)
    parser.add_argument("--properties", dest="properties",
                        help="Path to properties file", metavar="FILE", required=True)
    parser.add_argument("--tmpdir", dest="tmpdir",
                        help="Path to temp directory", metavar="FILE", required=True)
    parser.add_argument("--trimreads", dest="trimreads",  
                        help="Trim reads in cutadapt step", metavar="True/False", required=True)

    # Arguments initialization
    args = parser.parse_args()
    inR1 = args.inR1
    inR2 = args.inR2
    outdir = args.outdir
    properties = args.properties
    barcode = args.barcode
    tmpdir = args.tmpdir
    trimreads = args.trimreads

    if not os.path.isfile(inR1):
        parser.error("Input file {} does not exist".format(inR1))
        sys.exit(1)
    if not os.path.isfile(inR2):
        parser.error("Input file {} does not exist".format(inR2))
        sys.exit(1)
    if not os.path.isdir(outdir):
        logging.debug("Output directory does not exist, going to create it")
        os.system("mkdir -p {}".format(outdir))
    if not os.path.isfile(properties):
        parser.error("Properties file does not exit")
        sys.exit(1)
    if barcode is None:
        parser.error("Barcode marker missing")
        sys.exit(1)
    if not os.path.isdir(tmpdir):
        logging.debug("Tmpdir directory does not exist, going to create it")
        os.system("mkdir -p {}".format(tmpdir))
    if trimreads:
        logging.debug("Going to trimreads at cutadapt step")

    return inR1, inR2, outdir, barcode, properties, tmpdir, trimreads


# get primers in properties file depending on barcode
def getPrimers(barcode, properties):
    config = configparser.ConfigParser()
    config.read(properties)
    if barcode in config:
        forward = []
        reverse = []
        for key in config[barcode]:
            if 'f' in key :
                forward.append(config[barcode][key])
            if 'r' in key :
                reverse.append(config[barcode][key])
        mismatch = config[barcode]["ALLOWED-MISMATCHES"]
        if forward is not None and reverse is not None and mismatch is not None:
            return forward, reverse, mismatch
    else:
        logging.debug("Barcode {} was not found in {}".format(
            barcode, properties))
        sys.exit(1)


# Use cutadapt in order to identify forward and reverse reads for each sample
# Then extract R1 and R2 to separate file for each sample
def extractReads(inR1, inR2, outdir, forward, reverse, mismatch, tmpdir, trimreads):

   R1F = "R1F"
   R1R = "R1R"
   R2R = "R2R"
   R2F = "R2F"
   if "_R1" in inR1 :
      couplename="{}".format(inR1.split("/")[-1].split(".fastq.gz")[0].replace("_R1", ""))
   else :
      couplename="{}".format(inR1.split("/")[-1].split(".fastq.gz")[0].replace("_1", ""))
   logging.debug("Couple name : {}".format(couplename))

   samplename="{}_{}".format(couplename, R1F)
   R1cutadaptoutnameF="{}/{}-cutadapt.fastq.gz".format(outdir, samplename)
   runCutadapt(forward, mismatch, R1cutadaptoutnameF, samplename, inR1, tmpdir, trimreads)
   cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/2$/s/\/2/\/1/g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R1cutadaptoutnameF, R1cutadaptoutnameF, R1cutadaptoutnameF, R1cutadaptoutnameF)
   out = runcmd(cmd)

   samplename="{}_{}".format(couplename, R2R)
   R2cutadaptoutnameR="{}/{}-cutadapt.fastq.gz".format(outdir, samplename)
   runCutadapt(reverse, mismatch, R2cutadaptoutnameR, samplename, inR2, tmpdir, trimreads)
   cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/1$/s/\/1/\/2/g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R2cutadaptoutnameR, R2cutadaptoutnameR, R2cutadaptoutnameR, R2cutadaptoutnameR)
   out = runcmd(cmd)

   singletons="{}/{}_R1F-R2Rsingletons.fastq.gz".format(outdir, couplename) 
   R1repairedF= R1cutadaptoutnameF.replace("-cutadapt", "")
   R2repairedR= R2cutadaptoutnameR.replace("-cutadapt", "")
   rePairReads(outdir, R1repairedF, R2repairedR, singletons, R1cutadaptoutnameF, R2cutadaptoutnameR)

   samplename="{}_{}".format(couplename, R1R)
   R1cutadaptoutnameR="{}/{}-cutadapt.fastq.gz".format(outdir,samplename)
   runCutadapt(reverse, mismatch, R1cutadaptoutnameR, samplename, inR1, tmpdir, trimreads)
   cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/2$/s/\/2/\/1/g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R1cutadaptoutnameR, R1cutadaptoutnameR, R1cutadaptoutnameR, R1cutadaptoutnameR)
   out = runcmd(cmd)

   samplename="{}_{}".format(couplename, R2F)
   R2cutadaptoutnameF="{}/{}-cutadapt.fastq.gz".format(outdir,samplename)
   runCutadapt(forward, mismatch, R2cutadaptoutnameF, samplename, inR2, tmpdir, trimreads)
   cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/1$/s/\/1/\/2/g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R2cutadaptoutnameF, R2cutadaptoutnameF, R2cutadaptoutnameF, R2cutadaptoutnameF)
   out = runcmd(cmd)
   
   singletons="{}/{}_R1R-R2Fsingletons.fastq.gz".format(outdir, couplename) 
   R1repairedR= R1cutadaptoutnameR.replace("-cutadapt", "")
   R2repairedF= R2cutadaptoutnameF.replace("-cutadapt", "")
   rePairReads(outdir, R1repairedR, R2repairedF , singletons, R1cutadaptoutnameR, R2cutadaptoutnameF)

   R1name=os.path.basename(R1repairedF.replace("_R1F", "_R1").replace("fastq.gz", "fastq"))
   R2name=os.path.basename(R1repairedR.replace("_R1R", "_R2").replace("fastq.gz", "fastq"))
   cmd = "zcat {} {} > {}/frogs/{} ; gzip {}/frogs/{}".format(R1repairedF, R2repairedF, outdir, R1name, outdir, R1name)
   out = runcmd(cmd)
   cmd = "zcat {} {} > {}/frogs/{} ; gzip {}/frogs/{}".format(R1repairedR, R2repairedR, outdir, R2name, outdir, R2name)
   out = runcmd(cmd)

def runCutadapt(primerseq, mismatch, outfile, samplename, infile, tmpdir, trimreads) :
    logging.debug("Going to run cutadapt on file {}".format(infile))
    if trimreads :
        action = "trim"
    else :
        action = "none"
    param = ""
    erate = ""
    minoverlap = ""
    for item in primerseq :
        param += "-g {} ".format(str(item))
        erate=round((int(mismatch) / int(len(item)) + 0.01), 2)
        minoverlap=int(len(item)) - 1

    if param is not None :
        cmd=". /appli/bioinfo/cutadapt/1.18/env.sh ; cutadapt --action={} -j $NCPUS {} -e {} -O {} --minimum-length=1 --max-n=0 --discard-untrimmed -o {} {} &> {}/cutadapt_{}.log;".format(action, param, erate, minoverlap, outfile, infile, tmpdir, samplename)
        out=runcmd(cmd)
    else :
        logging.debug("CUTADAPT ERROR while treating file {}, exiting programme".format(infile))
        sys.exit(1)

def rePairReads(outdir, R1repaired, R2repaired, singletons, R1file, R2file):
    # repair reads files generated with cutadapt in order have the same number of
    # reads in R1 and R2 files.
    # Remove singleton reads
    # Using bbmap/repair script

    # BBMAP cmd
    cmd=". /appli/bioinfo/bbmap/38.22/env.sh ; repair.sh overwrite=t in1={} in2={} out1={} out2={} outs={} repair;".format(
        R1file, R2file, R1repaired, R2repaired, singletons)
    out=runcmd(cmd)
    
    #Remove /1 and /2 in reads name in order to let dada2 read correctly the input files
    cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/1$/s/\/1//g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R1repaired, R1repaired, R1repaired, R1repaired)
    logging.debug("Removing /1 from file {} : {}".format(R1repaired, cmd))
    out=runcmd(cmd)
    cmd = "gunzip < {} | sed -e '/@[A-Z][0-9]:.*\/2$/s/\/2//g' | gzip -c > {}.tempo.gz ; mv {}.tempo.gz {}".format(R2repaired, R2repaired, R2repaired, R2repaired)
    logging.debug("Removing /2 from file {} : {}".format(R1repaired, cmd))
    out=runcmd(cmd)

def runcmd(cmd):
    logging.debug("Running process....")
    p=subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    logging.debug(cmd)
    p.wait()
    (out, err)=p.communicate()
    if isinstance(out, bytes):
        out=out.decode("utf-8")
    if p.returncode != 0 and err is not None:
        logging.debug(
            "p = {} -- OUT = {} -- ERR = {}".format(str(p.returncode), out, err))
        logging.debug("ERROR : Process aborted")
        sys.exit(1)
    else:
        logging.debug("Process OK")
        return out

if __name__ == '__main__':

    # Handle script arguments
    inR1, inR2, outdir, barcode, properties, tmpdir, trimreads=handleArgs()
    # logging config
    st = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    logfile='{}/extractR1R2_{}_{}.log'.format(tmpdir,inR1.split(".fastq.gz")[0].split("/")[-1:][0], st)
    logging.basicConfig(level=logging.DEBUG, filename=logfile, format='%(asctime)s %(levelname)s %(message)s', filemode='w')
    
    logging.debug(
        "Extract forward and reverse reads from paired-end sequencing data")
    logging.debug("input R1 = {}".format(inR1))
    logging.debug("input R2 = {}".format(inR2))
    logging.debug("output directory = {}".format(outdir))
    logging.debug("tmp and log directory = {}".format(tmpdir))
    logging.debug("barcode = {}".format(barcode))
    logging.debug("properties = {}".format(properties))

    # get forward and reverse primers
    forward, reverse, mismatch = getPrimers(barcode, properties)
    logging.debug("Primer forward {} = {}".format(barcode, forward))
    logging.debug("Primer reverse {} = {}".format(barcode, reverse))

    cmd="mkdir -p {}/frogs".format(outdir)
    out=runcmd(cmd)
    # Extract Forward and Reverse Reads according to Primers (using cutadapt)
    extractReads(inR1, inR2, outdir, forward, reverse, mismatch, tmpdir, trimreads)

    logging.debug("End of processing")
