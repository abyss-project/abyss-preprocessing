# *Abyss-preprocessing*

## What for?

Extract and organize forward and reverse reads from ligation sequencing data

For Sample :
* R1 : sample_R1.fastq.gz or sample_X_1_X.fastq.gz
* R2 : sample_R2.fastq.gz or sample_X_2_X.fastq.gz

4 files will be generated :
* R1F : sample_R1F.fastq.gz for forward reads extracted from file R1
* R1R : sample_R1R.fastq.gz for reverse reads extracted from file R1
* R2F : sample_R2F.fastq.gz for forward reads extracted from file R2
* R2R : sample_R2R.fastq.gz for reverse reads extracted from file R2

## Requirements 
* The script will perform on raw sequencing files (fastq.gz) of one marker (ie : 18S-V1) for several samples
* properties.ini : containing forward and reverse primer sequences and number of allowed-mismatches to calculate cutadapt primer error rate for each marker
* extract.ini : Paths configuration and marker selection
* All the fastq files must be in the same input directory
* Python 3.6 is required

## Steps
* [Cutadapt](https://cutadapt.readthedocs.io/en/stable/) is run to separate reads matching the forward primer from reads matching to the reverse primer in raw sequencing files. For each file (R1 and R2), two files are created : R1F, R1R and R2F, R2R.
* Reads from file sample_R1R-cutadapt.tar.gz are all renamed with /2 extension instead of /1
* Reads from file sample_R2F-cutadapt.tar.gz are all renamed with /1 extension instead of /2
* Files sample_R1F-cutadapt.tar.gz and sample_R2R-cutadapt.tar.gz are re-paired using [BBMAP repair](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/repair-guide/) script in order to remove singletons and to sort.
* Files sample_R2F-cutadapt.tar.gz and sample_R1R-cutadapt.tar.gz are re-paired using [BBMAP repair](https://jgi.doe.gov/data-and-tools/bbtools/bb-tools-user-guide/repair-guide/) script in order to remove singletons.
* Reads names are modified : extensions /1 and /2 are removed in order to have the same name in file R1 than in file R2 which is a requirement for dada2 to recognize the pairs of reads.
* Final number of output files are checked to make sure that all samples have been processed and samples.tar archive is created for FROGS processing without dada2

Final output files are names : 
* sample_R1F.fastq.gz 
* sample_R1R.fastq.gz 
* sample_R2F.fastq.gz 
* sample_R2R.fastq.gz 
* frogs/samples.tar archive for frogs

## How to run (IF RUN OUTSIDE ABYSS-PIPELINE) ?
- **extract.ini** : configuration file to edit
  - DIRECTORY : current directory of pipeline
  - INPUT : path to the input directory containing the fastq.gz reads files of one marker for several samples.
  - OUTPUT : path to output directory
  - PROPERTIES : path to file containing forward and reverse primers for different markers
  - BARCODE : name of the marker (ie : 18S-V1) (this marker must be listed in the PROPERTIES file)
  - TRIMREADS : set to True if you want to perform all abyss preprocessing (cutadapt, bbmap) (option : True/False)


- **extract.sh** : script which will run extract.py on the configuration file **extract.ini**. Each sample (and its two "paired-end" files) will be parsed separately. This script is not used when abyss-preprocessing is run within abyss-pipeline, as extract.py is launched by main.sh

- **extract.py** : python script that will read extract.ini file and launch extractR1R2.pbs calculation on each sample. The check.pbs script is run once at the end to verify that all files are created at the end of the process and produce the FROGS archive.

- **extractR1R2.pbs** : pbs script that will run extractR1R2.py, which will execute the steps presented above (cutadapt, bbmap repair...)

- **check.pbs** : pbs script that will run check.py, which will verify that 5 files have been created for each sample and then produce the FROGS archive in ./frogs/samples.tar


```bash
./extract.sh
```

(c) 2020 - Ifremer
